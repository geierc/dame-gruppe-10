
#include "moves.h"

StepMove* StepMove_create(const Point* start,const Point* end){
	StepMove* stepMove=malloc(sizeof(StepMove));
	memcpy(&stepMove->start,start,sizeof(Point));
	memcpy(&stepMove->end,end,sizeof(Point));
	return stepMove;
}

JumpMove* JumpMove_create(const Point* start,const Point* enemy,const Point* end){
	JumpMove* jumpMove=malloc(sizeof(JumpMove));
	memcpy(&jumpMove->start,start,sizeof(Point));
	memcpy(&jumpMove->enemy,enemy,sizeof(Point));
	memcpy(&jumpMove->end,end,sizeof(Point));
	return jumpMove;
}

void applyStepMove(char playingField[8][8],const StepMove* move){
	const char piece=getVal(move->start,playingField);
	playingField[move->start.row][move->start.col]='*';
	playingField[move->end.row][move->end.col]=piece;
}

void applyJumpMove(char playingField[8][8],const JumpMove* move){
	const char piece=getVal(move->start,playingField);
	playingField[move->start.row][move->start.col]='*';
	playingField[move->end.row][move->end.col]=piece;
	playingField[move->enemy.row][move->enemy.col]='*';
}

void movePieceAndMark(char playingField[8][8],const JumpMove* move){
	const char piece=getVal(move->start,playingField);
	playingField[move->start.row][move->start.col]='*';
	playingField[move->end.row][move->end.col]=piece;
	playingField[move->enemy.row][move->enemy.col]='X';
}

void removeMark(char playingField[8][8],const JumpMove* move){
	playingField[move->enemy.row][move->enemy.col]='*';
}


int stepMovesForMen(const char playingField[8][8],const Point start,const bool white,StepMove* stepMoves[2]){
	int nMoves=0;
	const int rowDir= white ? -1 : 1;
	for(int left=0;left<2;left++){
		const int colDir=(left==0) ? -1 : 1;
		Point p2;
		p2.row=start.row+rowDir;
		p2.col=start.col+colDir;
		if(validate(p2) && getVal(p2,playingField)=='*'){
			//Step move found
			StepMove* stepMove=StepMove_create(&start,&p2);
			stepMoves[nMoves]=stepMove;
			nMoves++;
		}
	}
	return nMoves;
}

int stepMovesForQueen(const char playingField[8][8],const Point start,StepMove* stepMoves[64]){
	int nMoves=0;
	for(int dir=0;dir<4;dir++){
		int rowDir,colDir;
		switch(dir){
			case 0: rowDir=1; colDir=1;break;
			case 1: rowDir=1; colDir=-1;break;
			case 2: rowDir=-1; colDir=1;break;
			case 3: rowDir=-1; colDir=-1;break;
		}
		Point p2={start.row,start.col};
		bool canMove=true;
		while(canMove){
			p2.row+=rowDir;
			p2.col+=colDir;
			if(!validate(p2)){
				canMove=false;
				continue;
			}
			const char val=getVal(p2,playingField);
			if(val=='*'){
				//Step move found
				if(nMoves>=MAX_STEP_MOVES_PER_QUEEN){
					perror("Too many possible step moves for queen\n");
					return nMoves;
				}
				StepMove* stepMove=StepMove_create(&start,&p2);
				stepMoves[nMoves]=stepMove;
				nMoves++;
			}else{
				canMove=false;
			}
		}
	}
	return nMoves;
}


int jumpMovesForMen(const char playingField[8][8],const Point start,const bool white,JumpMove* jumpMoves[2]){
	Point enemy;
	int nMoves=0;
	const int rowDir= white ? -1 : 1;
	for(int left=0;left<2;left++){
		const int colDir= (left==0) ? -1 : 1;
		enemy.row=start.row+rowDir;
		enemy.col=start.col+colDir;
		if(validate(enemy) && isEnemy(getVal(enemy,playingField),white)){
			//change this is a capturing move
			//check if the next field is free
			Point end;
			end.row=enemy.row+rowDir;
			end.col=enemy.col+colDir;
			if(validate(end) && getVal(end,playingField)=='*'){
				//field is free, we have found a capturing move
				JumpMove* jumpMove=JumpMove_create(&start,&enemy,&end);
				jumpMoves[nMoves]=jumpMove;
				nMoves++;
			}
		}
	}
	return nMoves;
}

int jumpMovesForQueen(const char playingField[8][8],const Point start,const bool white,JumpMove* jumpMoves[4]){
	int nMoves=0;
	for(int dir=0;dir<4;dir++){
		int rowDir,colDir;
		switch(dir){
			case 0: rowDir=1; colDir=1;break;
			case 1: rowDir=1; colDir=-1;break;
			case 2: rowDir=-1; colDir=1;break;
			case 3: rowDir=-1; colDir=-1;break;
		}
		Point p2={start.row,start.col};
		bool canMove=true;
		while(canMove){
			p2.row+=rowDir;
			p2.col+=colDir;
			if(!validate(p2)){
				canMove=false;
				continue;
			}
			const char val=getVal(p2,playingField);
			if(val=='*'){
				continue;
			}else if(isEnemy(val,white)){
				Point p3={p2.row+rowDir,p2.col+colDir};
				//check if the next field is free
				if(validate(p3) && getVal(p3,playingField)=='*'){
					//Jump move found
					JumpMove* jumpMove=JumpMove_create(&start,&p2,&p3);
					jumpMoves[nMoves]=jumpMove;
					nMoves++;
				}
				canMove=false;
				continue;
			}else{
				canMove=false;
			}
		}
	}
	return nMoves;
}





