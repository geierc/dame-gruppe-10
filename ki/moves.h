#ifndef CONSTI_MOVES_H
#define CONSTI_MOVES_H

#include "helper.h"

#define MAX_STEP_MOVES_PER_QUEEN (16+2) //shouldn't be more than 16, but just to be safe add 2 more

//Found out by experiment: We play by the rule 'flying king'
//who may move any distance along unblocked diagonals, and capturing stones by jumping directly
//behind them
//		Col 0 | Col 1 | Col 2
//Row 0
//Row 1
//Row 2
//V*/

typedef struct { 
	Point start;
	Point end;
} StepMove;

typedef struct {
	Point start;
	Point enemy;
	Point end;
} JumpMove;


typedef struct {
	Point start;
	Point* moves[64];
}JumpMove2;


/*
 * Create a step move from 2 Points. Uses malloc !
**/
StepMove* StepMove_create(const Point* start,const Point* end);

/*
 *Create a jump move from 3 points. Uses malloc !
**/
JumpMove* JumpMove_create(const Point* start,const Point* enemy,const Point* end);

/*
 * Apply a step move to the playing field. Move piece from start to end.
**/
void applyStepMove(char playingField[8][8],const StepMove* move);

/*
 * Apply a jump move to the playing field. Move piece from start to end and kill enemy.
**/
void applyJumpMove(char playingField[8][8],const JumpMove* move);

/*
 * Move piece from start to end and mark enemy 'to be killed', with an 'X'
 * This is because stones are only removed from the playing field after a (multi-jump-move)
 * has been fully executed
**/
void movePieceAndMark(char playingField[8][8],const JumpMove* move);

/*
 * Remove mark from the playing field
**/
void removeMark(char playingField[8][8],const JumpMove* move);


/*
 * Calculate all possible step moves for one men
 * Up to 2 step moves are possible per piece
 * playingField: the playing field with men at Position start
 * start: The position of the men on the playing field
 * white: determines if the piece moves up- or downwards
 * stepMoves: Contains results on succes
 * @return: the number of step moves, 0 when piece cannot move
 **/
int stepMovesForMen(const char playingField[8][8],const Point start,const bool white,StepMove* stepMoves[2]);

/*
 * like @stepMovesForMen, just for queen
 * This time, up to ? moves  are possible -> just to be safe i use 64. Maybe a littlebit too many
**/
int stepMovesForQueen(const char playingField[8][8],const Point start,StepMove* stepMoves[MAX_STEP_MOVES_PER_QUEEN]);


/*
 * Calculates all possible jump moves for one men,
 * without taking multi-jump into account
 * Up to 2 jump moves are possible per piece
 * playingField: the playing field with men at Position start
 * start: The position of the men on the playing field
 * white: determines if the piece moves up or downwards
 * jumpMoves: Contains results on succes
 * @return: the number of jump moves, 0 when piece cannot move
**/
int jumpMovesForMen(const char playingField[8][8],const Point start,const bool white,JumpMove* jumpMoves[2]);


/*
 * like @jumpMovesForMen, just for queen
 * This time, up to 4 moves are possible
**/
int jumpMovesForQueen(const char playingField[8][8],const Point start,const bool white,JumpMove* jumpMoves[4]);







#endif