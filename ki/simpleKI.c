#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <signal.h>
#include <time.h>
#include <limits.h>

#include "moves.h"
#include "simpleKI.h"
#include "heuristic.h"



void generateMoveCommand(char* move,const Point p1,const Point p2){
	move[0]=convertCol(p1); //
	move[1]=convertRow(p1);
	move[2]=':';
	move[3]=convertCol(p2);
	move[4]=convertRow(p2);
	move[5]='\0';
}

void appendMoveCommand(char* move,const Point p1){
	const int len=strlen(move);
	move[len+0]=':';
	move[len+1]=convertCol(p1); //
	move[len+2]=convertRow(p1);
	move[len+3]='\0';
}


void simple_selectMove(Table* now, char* moveCommandB);
void random_selectMove(Table* now, char* moveCommandB);
void weighted_selectMove(Table* now, char* moveCommandB);


void calculateNewMove(const char playingField[8][8], char* moveCommandB,const bool white,const int mode){
	//printf("calculateNewMove() Begin\n");
	clock_t begin = clock();
	
	Table now;
	T_Prepare(&now);
	memcpy(now.playingField,playingField,sizeof(char)*8*8);
	now.white=white;
	T_calculateAllMoves(&now);
	T_printStepMoves(&now);
	T_printJumpMoves(&now);
	
	if(mode==0){
		simple_selectMove(&now,moveCommandB);
	}else if(mode==1){
		random_selectMove(&now,moveCommandB);
	}else{
		weighted_selectMove(&now,moveCommandB);
	}
	
	T_Cleanup(&now);
	
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Time spent:%lf\n",time_spent);
	//printf("calculateNewMove() End\n");
}


//just select the first jump move (if possible)
//else the first step move
void simple_selectMove(Table* now, char* moveCommandB){
	const int nJumpMoves=kv_size(now->jumpMoves);
	if(nJumpMoves>0){
		TMove* selected=kv_A(now->jumpMoves,0);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
		for(int i=1;i<selected->stepC;i++){
			appendMoveCommand(moveCommandB,selected->steps[i]);
		}
	}else{
		TMove* selected=kv_A(now->stepMoves,0);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
	}
}

//select a random jump/step move
//usefull for testing
void random_selectMove(Table* now, char* moveCommandB){
	time_t t;
    srand((unsigned) time(&t));//rand() % 50 -> number between 0 and 49
	
	const int nJumpMoves=kv_size(now->jumpMoves);
	const int nStepMoves=kv_size(now->stepMoves);
	if(nJumpMoves>0){
		const int idx= rand() % nJumpMoves;
		printf("J Move %d\n",idx);
		TMove* selected=kv_A(now->jumpMoves,idx);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
		for(int i=1;i<selected->stepC;i++){
			appendMoveCommand(moveCommandB,selected->steps[i]);
		}
	}else{
		const int idx= rand() % nStepMoves;
		printf("S Move %d\n",idx);
		TMove* selected=kv_A(now->stepMoves,idx);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
	}
}


//select the best jump / best step move possible,
//based on the lookup table
void weighted_selectMove(Table* now, char* moveCommandB){
	const int nJumpMoves=kv_size(now->jumpMoves);
	const int nStepMoves=kv_size(now->stepMoves);
	
	if(nJumpMoves>0){
		//higher score = jump move removes more enemy pieces
		int currBestIdx=0;
		int currBestScore=0;
		for(int i=0;i<nJumpMoves;i++){
			TMove* move=kv_A(now->jumpMoves,i);
			const int score=move->enemyC;
			if(score>currBestScore){
				currBestScore=score;
				currBestIdx=i;
			}
		}
		TMove* selected=kv_A(now->jumpMoves,currBestIdx);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
		for(int i=1;i<selected->stepC;i++){
			appendMoveCommand(moveCommandB,selected->steps[i]);
		}
	}else{
		//Take the move with the highest score for you,
		//that means your pieces have a good position according to the heuristics
		int currBestIdx=0;
		int currBestScore=0;
		for(int i=0;i<nStepMoves;i++){
			TMove* move=kv_A(now->stepMoves,i);
			const int score=heuristic_advanced(move->playingField,now->white);
			if(score>currBestScore){
				currBestScore=score;
				currBestIdx=i;
			}
		}
		TMove* selected=kv_A(now->stepMoves,currBestIdx);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
	}
}


/*
int baseAdvantage=
		for(int i=0;i<nJumpMoves;i++){
			TMove* move=kv_A(now.jumpMoves,i);
			const int scoreYou=heuristic_advanced(move->playingField,white);
			const int scoreEnemy=heuristic_advanced(move->playingField,!white);
			const int yourAdvantage=scoreYou-scoreEnemy;
			if(yo
		}
*/

/*
void calculateNewMove(const char playingField[8][8], char* moveCommandB,const bool white){
	printf("calculateNewMove() Begin\n");
	clock_t begin = clock();
	
	Table now;
	T_Prepare(&now);
	memcpy(now.playingField,playingField,sizeof(char)*8*8);
	now.white=white;
	T_calculateAllMoves(&now);
	T_printStepMoves(&now);
	T_printJumpMoves(&now);
	
	
	const bool randomize=true;
	time_t t;
    srand((unsigned) time(&t));
	//rand() % 50 -> number between 0 and 49
	
	const int nJumpMoves=kv_size(now.jumpMoves);
	const int nStepMoves=kv_size(now.stepMoves);
	
	
	if(nJumpMoves>0){
		const int idx= randomize ? (rand() % nJumpMoves) : 0;
		printf("J Move %d\n",idx);
		TMove* selected=kv_A(now.jumpMoves,idx);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
		for(int i=1;i<selected->stepC;i++){
			appendMoveCommand(moveCommandB,selected->steps[i]);
		}
	}else{
		const int idx= randomize ? (rand() % nStepMoves) : 0;
		printf("S Move %d\n",idx);
		TMove* selected=kv_A(now.stepMoves,idx);
		generateMoveCommand(moveCommandB,selected->start,selected->steps[0]);
	}
	
	T_Cleanup(&now);
	
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Time spent:%lf\n",time_spent);
	printf("calculateNewMove() End\n");
}
*/

