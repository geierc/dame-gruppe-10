#define _POSIX_C_SOURCE 200809L
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <getopt.h>

#include "config.h"


/**
 * Used by both parsing approaches.
 * Set values in the config_t struct.
 * Input: tuple of {value,param}
**/
void checkParameterNames(char* name,char* value){
	if(strcmp(name,"HOSTNAME")==0){
		printf("Hostname: %s\n",value);
        sscanf(value,"%s",config.Hostname);
	} else if(strcmp(name,"PORTNUMBER")==0){
		printf("Portnumber: %s\n",value);
        sscanf(value,"%d",&config.Portnumber);
	} else if(strcmp(name,"GAMEKIND")==0){
		printf("Gamekind: %s\n",value);
        sscanf(value,"%s",config.Gamekind);
	} else{
		printf("Error unimplemented value\n");
	}
}

/*
 * New idea to parse
 * use sscanf to split the input string in two parts
 * */
void parseConfig(char* PATH){
    FILE* file;
    if((file = fopen(PATH, "r")) != NULL){
        printf("ConfigFile opened\n");
        char *buffer=NULL;
        size_t buflen=0;
        ssize_t nread;
        while((nread=getline(&buffer,&buflen,file))!=-1){
            const int lineLength=strlen(buffer);
            char befEq[lineLength];
            char aftEq[lineLength];
            char befEq1[lineLength];
            sscanf(buffer,"%[^=]= %s",befEq1,aftEq);
            sscanf(befEq1," %s",befEq);
            checkParameterNames(befEq,aftEq);
            //free(buffer);
        }
        free(buffer);
        fclose(file);
    }else{
		perror("Cannot open config file\n");
		exit(EXIT_FAILURE);
	}
}

void removeBlanks(char* buff,int len){
	char tmpBuff[len];
	int counter=0;
	for(int i=0;i<len;i++){
		if(buff[i]!=' ' && buff[i]!='\n'){ //also get rid of the '\n' symbol
			tmpBuff[counter]=buff[i];
			counter++;
		}
	}
	memcpy(buff,tmpBuff,counter);
	//manually add the string terminator (stlrlen will work afterwards)
	buff[counter]='\0';
}

//different logic than before:
//Here, we read line for line from the file
//and then search for the '=' symbol.
//Once we found it, we can divide the line into two parts
//the part before the '=' ( parameter name and optional blanks)
//the part after the '=' (parameter value and optional blanks)
//After splitting into two parts we only have to remove all blanks,
//then we are done, and can check the parameter names against the values we have implemented.
//Compared to #1 this has much less overhead when adding new values and
//also sucesfully deals with the blanks
void parseConfig_old(char* PATH){
	FILE* file;
    if((file = fopen(PATH,"r"))!=NULL){
		printf("File open\n");
		char *buffer=NULL;
        size_t buflen=0;
        ssize_t nread;
        while((nread=getline(&buffer,&buflen,file))!=-1){
			
			const int lineLength=strlen(buffer);
			//printf("%s len:%d\n",buffer,lineLength);
			
			for(int i=0;i<lineLength;i++){
				if(buffer[i]=='='){
					
					char beforeEquals[i];
					memcpy(beforeEquals,buffer,i);
					removeBlanks(beforeEquals,i);
					//printf("%s\n",beforeEquals);
					
					//char* afterEquals=&buffer[i+1];
					const int afterEqualsLen=lineLength-i-1;
					char afterEquals[afterEqualsLen];
					memcpy(afterEquals,&buffer[i+1],afterEqualsLen);
					removeBlanks(afterEquals,afterEqualsLen);
					//printf("%s\n",afterEquals);
					
					checkParameterNames(beforeEquals,afterEquals);
					break;
				}
			}
			
		    //free(buffer);	
		}
        free(buffer);
		fclose(file);
	}
}


void printConfigStruct(config_t *config){
	printf("-----------\n");
	printf("Hostname: %s\n",config->Hostname);
	printf("Portnumber: %d\n",config->Portnumber);
	printf("Gamekind: %s\n",config->Gamekind);
	printf("-----------\n");
}




