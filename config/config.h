#define MAX_CONFIG_LINE_LENGTH 256 //maximum length of one line / the read buffer

typedef struct{
	char Hostname[256];
    int Portnumber;
	char Gamekind[256];
} config_t;

config_t config;

//void parseConfig(char* PATH,config_t *config);
void parseConfig(char* PATH);
void parseConfig_old(char* PATH);
void printConfigStruct(config_t *config);
