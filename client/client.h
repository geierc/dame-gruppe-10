#define _POSIX_C_SOURCE_ 2
#define _POSIX_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <getopt.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>


typedef struct {
    char gamename[64];
    int player;
    int nplayers;
    pid_t connectorid;
    pid_t thinkerid;
	bool shouldThink;
	char playingField[8][8];
} shmContent_t;


typedef struct {
	char GAME_ID[64];
	int PLAYER;
	int mKIMode;
	char mConfigFileName[64*2];
} cdLineOptions_t;

cdLineOptions_t cdLineOptions;

shmContent_t *shmContents;

int fd_pipe[2];

void printPlayingField(char playingField[8][8]);

void concat2(const char *s1, const char *s2,char* result);




