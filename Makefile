##tut from cs.colby.edu

CC=gcc
CFLAGS=  -std=c99 -Wall -Wextra -Werror 
LDFlags = 
TARGET = sysprak-client
SRCFILES = client/client.c client/client.h client/performConnection.c client/performConnection.h \
config/config.c config/config.h \
ki/simpleKI.c ki/simpleKI.h ki/moves.c ki/moves.h ki/helper.c ki/helper.h ki/kvec.h ki/table.c ki/table.h \
ki/heuristic.c ki/heuristic.h 


sysprak-client: $(SRCFILES)
	$(CC) $(CFLAGS) $(SRCFILES) -o $(TARGET) 

play: $(TARGET) 
	./$(TARGET)   -g $(GAME_ID) -p $(PLAYER)


clean:
	rm -f $(TARGET) 
